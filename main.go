package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"sort"
)

type ExchangeInfo struct {
	Last   float64 `json:"last"`
	Symbol string  `json:"symbol"`
}

type ExchangeResponse map[string]ExchangeInfo

func versionHandler(w http.ResponseWriter, r *http.Request) {
	data, _ := ioutil.ReadFile("VERSION")
	fmt.Fprintf(w, string(data))
}

func getExchangeInfo() (*ExchangeResponse, error) {
	resp, err := http.Get("https://blockchain.info/ticker")
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	xchgResp := &ExchangeResponse{}
	if err := json.NewDecoder(resp.Body).Decode(xchgResp); err != nil {
		return nil, err
	}

	return xchgResp, nil
}

func exchangeHandler(w http.ResponseWriter, r *http.Request) {
	exchangeInfo, err := getExchangeInfo()
	if err != nil {
		http.Error(w, err.Error(), 503)
		return
	}

	currencies := []string{}
	info := *exchangeInfo

	for currency, _ := range info {
		currencies = append(currencies, currency)
	}

	sort.Strings(currencies)

	for _, currency := range currencies {
		fmt.Fprintf(w, "Currency: %s. 1 BTC = %.2f\n", currency, info[currency].Last)
	}
}

func main() {
	mux := http.NewServeMux()

	mux.Handle("/version", http.HandlerFunc(versionHandler))
	mux.Handle("/exchange", http.HandlerFunc(exchangeHandler))

	s := &http.Server{
		Handler: mux,
		Addr:    ":8080",
	}

	log.Fatal(s.ListenAndServe())
}
