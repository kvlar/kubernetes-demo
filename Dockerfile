FROM golang:alpine3.7

COPY . /go/app
WORKDIR /go/app
RUN go build -o server.x
EXPOSE 8080

ENTRYPOINT ["/go/app/server.x"]
